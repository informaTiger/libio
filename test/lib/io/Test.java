package lib.io;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;

public class Test {

    public static void main(String[] args) throws Exception{
        String str;
        
        try (SeekableInputStream sfs = new SeekableInputStream("build.xml")){
            byte[] buffer = new byte[1024];
            int len;
            long total = 0;

            System.out.println("available: " + sfs.available());
            System.out.println("file pointer (fpos): " + sfs.getPosition());
            sfs.setPosition(389);
            System.out.println("file pointer (fpos): " + sfs.getPosition());
            long skipped = sfs.skip(4096);
            System.out.println("skipped bytes: " + skipped);
            System.out.println("file pointer (fpos): " + sfs.getPosition());
            sfs.setPosition(0);

            ByteArrayOutputStream stream = new ByteArrayOutputStream();
            while ((len = sfs.read(buffer)) > 0){
                stream.write(buffer, 0, len);
                total += len;
            }
            str = new String(stream.toByteArray());
            stream.close();

            System.out.println(str);
            System.out.println("Total bytes read: " + total);
        }
        
        try (SeekableOutputStream sfs = new SeekableOutputStream("a.out")){
            byte[] buffer = new byte[1024];
            int len;
            long total = 0;
            
            ByteArrayInputStream stream = new ByteArrayInputStream(str.getBytes());
            while ((len = stream.read(buffer)) > 0){
                sfs.write(buffer, 0, len);
                total += len;
            }
            stream.close();
            
            System.out.println("Total bytes written: " + total);
        }
    }
}
