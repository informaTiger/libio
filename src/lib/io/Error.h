#include <jni.h>

#ifndef Error_H
#define Error_H
#ifdef __cplusplus
extern C {
#endif

jint throwNoClassDefError(JNIEnv* env, char* message);

jint throwFileNotFoundException(JNIEnv* env, char* message);

jint throwIOException(JNIEnv* env, char* message);
    
#ifdef __cplusplus
}
#endif
#endif