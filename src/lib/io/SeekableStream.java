/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lib.io;

import java.io.IOException;

/**
 *
 * @author Thomas
 */
public interface SeekableStream {
    
    public static enum Mode {
        
        READ("r"), READ_BINARY("rb"), READ_UPDATE("r+"), READ_UPDATE_BINARY("r+b"),
        WRITE("w"), WRITE_BINARY("wb"), WRITE_UPDATE("w+"), WRITE_UPDATE_BINARY("w+b");
    
        private final String id;
        
        Mode(String id){
            this.id = id;
        }
        
        public String id(){
            return id;
        }
        
        public static boolean isInvalid(Mode val, Mode... modes){
            for (Mode mode : modes){
                if (val.equals(mode)){
                    return true;
                }
            }
            return false;
        }
    }
    
    public long getPosition();
    
    public void setPosition(long position) throws IOException;
    
    public long getLength();
}