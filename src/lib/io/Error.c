#include "Error.h"

jint throwNoClassDefError(JNIEnv* env, char* message){
    jclass exClass;
    char* className = "java/lang/NoClassDefFoundError";

    exClass = (*env)->FindClass(env, className);
    if (exClass == NULL){
        return 0;
    }
    return (*env)->ThrowNew(env, exClass, message);
}

jint throwFileNotFoundException(JNIEnv* env, char* message){
    jclass exClass;
    char* className = "java/io/FileNotFoundException";

    exClass = (*env)->FindClass(env, className);
    if (exClass == NULL){
        return throwNoClassDefError(env, className);
    }
    return (*env)->ThrowNew(env, exClass, message);
}

jint throwIOException(JNIEnv* env, char* message){
    jclass exClass;
    char* className = "java/io/IOException";

    exClass = (*env)->FindClass(env, className);
    if (exClass == NULL){
        return throwNoClassDefError(env, className);
    }
    return (*env)->ThrowNew(env, exClass, message);
}
