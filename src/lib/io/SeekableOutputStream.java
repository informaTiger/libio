/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lib.io;

import java.io.File;
import java.io.IOException;
import java.io.OutputStream;

public class SeekableOutputStream extends OutputStream implements SeekableStream {
    
    public SeekableOutputStream(String path){
        this(new File(path));
    }
    
    public SeekableOutputStream(File file){
        open(file.getAbsolutePath(), Mode.WRITE_BINARY);
    }
    
    public SeekableOutputStream(String path, Mode mode){
        this(new File(path), mode);
    }
    
    public SeekableOutputStream(File file, Mode mode){
        checkMode(mode);
        open(file.getAbsolutePath(), mode);
    }
    static {
        System.loadLibrary("io");
    }

    @Override
    public long getPosition(){
        return fget();
    }
    
    @Override
    public void setPosition(long position) throws IOException{
        fset(position);
    }

    @Override
    public long getLength() {
        return length();
    }
    
    @Override
    public void write(int b) throws IOException{
        write0(b);
    }

    @Override
    public void write(byte[] b, int off, int len) throws IOException{
        if (b == null){
            throw new NullPointerException("b == null");
        } else if (off < 0 || len < 0 || off + len > b.length){
            throw new IndexOutOfBoundsException();
        } else if (len == 0){
            return;
        }
        write0(b, off, len);
    }

    @Override
    public void close() throws IOException{
        close0();
    }
    
    private void checkMode(Mode mode){
        if (Mode.isInvalid(mode, Mode.READ, Mode.READ_BINARY)){
            throw new IllegalArgumentException(String.format(
                    "Mode %s is not supported by %s",
                    mode.id(),
                    getClass().getSimpleName()
                )
            );
        }
    }
    
    private native void open(String path, Mode mode);
    
    private native long fget();
    
    private native void fset(long offset);
    
    private native long length();
    
    private native void write0(int b);
    
    private native void write0(byte[] b, int off, int len);
    
    private native void close0();
}
