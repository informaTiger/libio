#include <stdio.h>
#include "Error.h"
#include "SeekableOutputStream.h"

static FILE* fp;

JNIEXPORT void JNICALL Java_lib_io_SeekableOutputStream_open
  (JNIEnv *env, jobject obj, jstring path, jobject mode){
    jclass ModeClass = (*env)->GetObjectClass(env, mode);
    jfieldID modeId = (*env)->GetFieldID(env, ModeClass, "id", "Ljava/lang/String;");
    
    jstring modeStr = (*env)->GetObjectField(env, mode, modeId);
    
    const char* nativePath = (*env)->GetStringUTFChars(env, path, 0);
    const char* nativeMode = (*env)->GetStringUTFChars(env, modeStr, 0);
    
    fp = fopen(nativePath, nativeMode);
    if (fp == NULL){
        throwFileNotFoundException(env, "File does not exist or could not be opened.");
    }    
    (*env)->ReleaseStringUTFChars(env, path, nativePath);
    (*env)->ReleaseStringUTFChars(env, modeStr, nativeMode);
}

JNIEXPORT jlong JNICALL Java_lib_io_SeekableOutputStream_fget
  (JNIEnv *env, jobject obj){
    return ftell(fp);    
}

JNIEXPORT void JNICALL Java_lib_io_SeekableOutputStream_fset
  (JNIEnv *env, jobject obj, jlong offset){
    if (fseek(fp, offset, SEEK_SET)){
        throwIOException(env, "Could not seek to offset.");
    }
}

JNIEXPORT jlong JNICALL Java_lib_io_SeekableOutputStream_length
  (JNIEnv *env, jobject obj){
    long cur = ftell(fp);
    
    if (fseek(fp, 0, SEEK_END)){
        return throwIOException(env, "SEEK_END is not supported.");
    }
    long end = ftell(fp);
    
    if (fseek(fp, cur, SEEK_SET)){
        return throwIOException(env, "Could not reset file pointer after SEEK_END.");
    }    
    return end;
}

JNIEXPORT void JNICALL Java_lib_io_SeekableOutputStream_write0__I
  (JNIEnv *env, jobject obj, jint b){
    char buffer = b;
    
    fwrite(&buffer, sizeof(char), 1, fp);
    if (ferror(fp)){
        throwIOException(env, "Could not write byte to file stream.");
    }    
}

JNIEXPORT void JNICALL Java_lib_io_SeekableOutputStream_write0___3BII
  (JNIEnv *env, jobject obj, jbyteArray buf, jint off, jint len){
    char* buffer = (char*)(*env)->GetByteArrayElements(env, buf, 0);
    
    fwrite(buffer + off, sizeof(char), len - off, fp);
    if (ferror(fp)){
        throwIOException(env, "Could not write bytes to file stream.");
    }    
    (*env)->ReleaseByteArrayElements(env, buf, (jbyte*)buffer, JNI_ABORT);
}

JNIEXPORT void JNICALL Java_lib_io_SeekableOutputStream_close0
  (JNIEnv *env, jobject obj){
    fclose(fp);
}
