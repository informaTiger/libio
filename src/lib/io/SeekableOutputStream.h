/* DO NOT EDIT THIS FILE - it is machine generated */
#include <jni.h>
/* Header for class lib_io_SeekableOutputStream */

#ifndef _Included_lib_io_SeekableOutputStream
#define _Included_lib_io_SeekableOutputStream
#ifdef __cplusplus
extern "C" {
#endif
/*
 * Class:     lib_io_SeekableOutputStream
 * Method:    open
 * Signature: (Ljava/lang/String;Llib/io/SeekableStream/Mode;)V
 */
JNIEXPORT void JNICALL Java_lib_io_SeekableOutputStream_open
  (JNIEnv *, jobject, jstring, jobject);

/*
 * Class:     lib_io_SeekableOutputStream
 * Method:    fget
 * Signature: ()J
 */
JNIEXPORT jlong JNICALL Java_lib_io_SeekableOutputStream_fget
  (JNIEnv *, jobject);

/*
 * Class:     lib_io_SeekableOutputStream
 * Method:    fset
 * Signature: (J)V
 */
JNIEXPORT void JNICALL Java_lib_io_SeekableOutputStream_fset
  (JNIEnv *, jobject, jlong);

/*
 * Class:     lib_io_SeekableOutputStream
 * Method:    length
 * Signature: ()J
 */
JNIEXPORT jlong JNICALL Java_lib_io_SeekableOutputStream_length
  (JNIEnv *, jobject);

/*
 * Class:     lib_io_SeekableOutputStream
 * Method:    write0
 * Signature: (I)V
 */
JNIEXPORT void JNICALL Java_lib_io_SeekableOutputStream_write0__I
  (JNIEnv *, jobject, jint);

/*
 * Class:     lib_io_SeekableOutputStream
 * Method:    write0
 * Signature: ([BII)V
 */
JNIEXPORT void JNICALL Java_lib_io_SeekableOutputStream_write0___3BII
  (JNIEnv *, jobject, jbyteArray, jint, jint);

/*
 * Class:     lib_io_SeekableOutputStream
 * Method:    close0
 * Signature: ()V
 */
JNIEXPORT void JNICALL Java_lib_io_SeekableOutputStream_close0
  (JNIEnv *, jobject);

#ifdef __cplusplus
}
#endif
#endif
