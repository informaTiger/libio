#include <stdio.h>
#include "Error.h"
#include "SeekableInputStream.h"

static FILE* fp;

JNIEXPORT void JNICALL Java_lib_io_SeekableInputStream_open
  (JNIEnv *env, jobject obj, jstring path, jobject mode){
    jclass ModeClass = (*env)->GetObjectClass(env, mode);
    jfieldID modeId = (*env)->GetFieldID(env, ModeClass, "id", "Ljava/lang/String;");
    
    jstring modeStr = (*env)->GetObjectField(env, mode, modeId);
    
    const char* nativePath = (*env)->GetStringUTFChars(env, path, 0);
    const char* nativeMode = (*env)->GetStringUTFChars(env, modeStr, 0);
    
    fp = fopen(nativePath, nativeMode);
    if (fp == NULL){
        throwFileNotFoundException(env, "File does not exist or could not be opened.");
    }
    (*env)->ReleaseStringUTFChars(env, path, nativePath);
    (*env)->ReleaseStringUTFChars(env, modeStr, nativeMode);
}

JNIEXPORT jlong JNICALL Java_lib_io_SeekableInputStream_fget
  (JNIEnv *env, jobject obj){
    return ftell(fp);
}

JNIEXPORT void JNICALL Java_lib_io_SeekableInputStream_fset
  (JNIEnv *env, jobject obj, jlong offset){
    if (fseek(fp, offset, SEEK_SET)){
        throwIOException(env, "Could not seek to offset.");
    }
}

JNIEXPORT jlong JNICALL Java_lib_io_SeekableInputStream_length
  (JNIEnv *env, jobject obj){
    long cur = ftell(fp);

    if (fseek(fp, 0, SEEK_END)){
        return throwIOException(env, "SEEK_END is not supported.");
    }
    long end = ftell(fp);

    if (fseek(fp, cur, SEEK_SET)){
        return throwIOException(env, "Could not reset file pointer after SEEK_END.");
    }
    return end;
}

JNIEXPORT jint JNICALL Java_lib_io_SeekableInputStream_read0__
  (JNIEnv *env, jobject obj){
    char buffer;

    if (feof(fp)){
        return -1;
    }
    fread(&buffer, sizeof(char), 1, fp);

    if (ferror(fp)){
        return throwIOException(env, "Could not read byte from file stream.");
    }
    return buffer;
}

JNIEXPORT jint JNICALL Java_lib_io_SeekableInputStream_read0___3BII
  (JNIEnv *env, jobject obj, jbyteArray buf, jint off, jint len){
    if (feof(fp)){
        return -1;
    }
    char* buffer[len];

    size_t size = fread(buffer, sizeof(char), len, fp);
    if (ferror(fp)){
        return throwIOException(env, "Could not read bytes from file stream.");
    }
    (*env)->SetByteArrayRegion(env, buf, off, size, (jbyte*)buffer);

    return size;
}

JNIEXPORT jlong JNICALL Java_lib_io_SeekableInputStream_seek
  (JNIEnv *env, jobject obj, jlong n){
    long cur = ftell(fp);
    long length = Java_lib_io_SeekableInputStream_length(env, obj);

    if (length <= cur + n){
        n = length - cur;
    }

    if (fseek(fp, n, SEEK_CUR)){
        throwIOException(env, "Could not skip bytes in file stream.");
    }
    return ftell(fp) - cur;
}

JNIEXPORT void JNICALL Java_lib_io_SeekableInputStream_close0
  (JNIEnv *env, jobject obj){
    fclose(fp);
}
