/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lib.io;

import java.util.Iterator;

/**
 *
 * @author Thomas
 */
public class ByteBuffer implements Iterable<Byte> {
    
    private static final int BUFFER_SPACE = 16;
    
    private byte[] data;
    
    private int pos;
    
    public ByteBuffer(){
        this(BUFFER_SPACE);
    }
    
    public ByteBuffer(int capacity){
        data = new byte[capacity];
    }
    
    public ByteBuffer(byte[] buf){
        data = new byte[buf.length + BUFFER_SPACE];
        System.arraycopy(buf, 0, data, 0, buf.length);
        
        pos = buf.length;
    }
    
    private static class ByteIterator implements Iterator<Byte> {
        
        private final byte[] buffer;
        
        private int pos;
        
        public ByteIterator(byte[] buf){
            buffer = buf;
            pos = 0;
        }
        
        @Override
        public boolean hasNext() {
            return pos < buffer.length;
        }
        
        @Override
        public Byte next() {
            return buffer[pos++];
        }
    }
    
    public void put(byte b){
        if (pos + 1 >= data.length){
            realloc();
        }
        data[pos++] = b;
    }
    
    public void put(int i){
        put((byte)i);
    }
    
    public void put(byte[] buf){
        if (pos + buf.length >= data.length){
            realloc(data.length + buf.length + BUFFER_SPACE);
        }
        
        for (int i = 0; i < buf.length; i++){
            data[pos++] = buf[i];
        }
    }
    
    public byte get(int index){
        if (index < 0 || index > pos){
            throw new IndexOutOfBoundsException("Index must not be negative and has to be smaller than actual size");
        }
        return data[index];
    }
    
    public ByteBuffer subBuffer(int end){
        return subBuffer(0, end);
    }

    public ByteBuffer subBuffer(int start, int end){
        return new ByteBuffer(toArray(start, end));
    }
    
    public int size(){
        return pos + 1;
    }
    
    @Override
    public Iterator<Byte> iterator() {
        return new ByteIterator(data);
    }
    
    public byte[] toArray(){
        byte[] arr = new byte[pos];
        System.arraycopy(data, 0, arr, 0, pos);
        
        return arr;
    }
    
    private byte[] toArray(int start, int end){
        if (start < 0 || end < 0){
            throw new IndexOutOfBoundsException("Start and end index must be positive");
        }
        if (end - start < 1){
            throw new IllegalArgumentException("Subbuffer must be at least 1 byte long");
        }
        byte[] arr = new byte[end - start];
        System.arraycopy(data, start, arr, 0, end - start);
        
        return arr;
    }
    
    private void realloc(){
        realloc((int)(Math.log(data.length) / Math.log(2)) + data.length);
    }
    
    private void realloc(int capacity){
        if (capacity <= data.length){
            throw new IllegalArgumentException("capacity <= length");
        }        
        byte[] tmp = new byte[capacity];
        System.arraycopy(data, 0, tmp, 0, data.length);
        
        data = tmp;
    }
}
