package lib.io;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;

public class SeekableInputStream extends InputStream implements SeekableStream {
    
    public SeekableInputStream(String path){
        this(new File(path));
    }
    
    public SeekableInputStream(File file){
        open(file.getAbsolutePath(), Mode.READ_BINARY);
    }
    
    public SeekableInputStream(String path, Mode mode){
        this(new File(path), mode);
    }
    
    public SeekableInputStream(File file, Mode mode){
        checkMode(mode);
        open(file.getAbsolutePath(), mode);
    }
    static {
        System.loadLibrary("io");
    }
    
    @Override
    public long getPosition(){
        return fget();
    }

    @Override
    public void setPosition(long position) throws IOException{
        fset(position);
    }
    
    @Override
    public long getLength() {
        return length();
    }

    @Override
    public int read() throws IOException{
        return read0();
    }

    @Override
    public int read(byte[] b, int off, int len) throws IOException{
        if (b == null){
            throw new NullPointerException("b == null");
        } else if (off < 0 || len < 0 || len > b.length - off){
            throw new IndexOutOfBoundsException();
        } else if (len == 0){
            return 0;
        }
        return read0(b, off, len);
    }

    @Override
    public long skip(long n) throws IOException{
        return seek(n);
    }

    @Override
    public void close() throws IOException{
        close0();
    }
    
    private void checkMode(Mode mode){
        if (Mode.isInvalid(mode, Mode.WRITE, Mode.WRITE_BINARY, Mode.WRITE_UPDATE, Mode.WRITE_UPDATE_BINARY)){
            throw new IllegalArgumentException(String.format(
                    "Mode %s is not supported by %s",
                    mode.id(),
                    getClass().getSimpleName()
                )
            );
        }
    }

    private native void open(String path, Mode mode);

    private native long fget();

    private native void fset(long offset);
    
    private native long length();

    private native int read0();

    private native int read0(byte[] b, int off, int len);

    private native long seek(long n);

    private native void close0();
}
